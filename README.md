# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Webserver to host some ToG Stuff
* Version 0.1

### How do I get set up? ###

* [pip install pipenv](https://pipenv.kennethreitz.org/en/latest/)
* git pull this repository.
* Run "pipenv install --deploy" in the downloaded repository location, where the Pipfile.lock is located.

### Who do I talk to? ###

* Dave Fenwick - dfenwick@gmail.com