import json
from flask import request, abort, Blueprint, current_app
from os.path import join


'''
API Zone

Accepting webhooks from bitbucket project for tog-discord bot.
Upon recieving valid push POST, restart the discord bot and pull
latest code from the repo.
'''


webhooks = Blueprint('webhooks', __name__)

@webhooks.route('/tog-bog/v1/push', methods=['POST'])
def push():
    if request.method == 'POST':
        try:
            headers = request.headers
            if headers['User-Agent'] == current_app.config['BIT_BUCKET_WH']:
                responce = request.get_json()
                if responce['push']['changes'][0]['old']['name'] == 'master':
                    #pull latest version of bot
                    print('pull latest version of bot')
            return '', 200
        except:
            return '', 500
    else:
        abort(400)