from flask import Flask


def create_app():
    app = Flask(__name__, 
                instance_relative_config=False,
                static_url_path='/static')

    app.config.from_object('config.Config')

    with app.app_context():
        from application.routes import main
        from application.webhooks.routes import webhooks

        app.register_blueprint(main)
        app.register_blueprint(webhooks)

        return app
