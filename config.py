import binascii
from os.path import join, dirname, abspath
from os import urandom, environ

class Config:
    # Placeholder - Store in .env
    SECRET_KEY = binascii.hexlify(urandom(24))
    # Dirs
    ROOT_DIR = dirname(abspath(__file__))
