from flask import Blueprint, render_template, current_app

main = Blueprint('main', __name__)

@main.route('/')
def home():
    return render_template('index.html',
                           title='ToG Apps Home')
